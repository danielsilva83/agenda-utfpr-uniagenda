package com.utfpr.uniagenda.domain.enumeration;

/**
 * The DiaSemana enumeration.
 */
public enum DiaSemana {
    Segunda_feira, Terca_feira, Quarta_feira, Quinta_feira, Sexta_feira
}
