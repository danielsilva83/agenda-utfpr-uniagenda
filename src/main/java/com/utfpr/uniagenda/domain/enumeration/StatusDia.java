package com.utfpr.uniagenda.domain.enumeration;

/**
 * The StatusDia enumeration.
 */
public enum StatusDia {
    Disponivel, Indisponivel
}
