/**
 * Data Access Objects used by WebSocket services.
 */
package com.utfpr.uniagenda.web.websocket.dto;
